package id.clorus.kebelet.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

public class LocationServiceManager {
	
	public static final String TAG = "LocationServiceManager";
	
	private long checkInterval = 30*60*1000;
	private LocationManager locationManager;
	private LocationListener locationListener;
	
	public LocationServiceManager(Context context, LocationListener locationListener) {
		this.locationListener = locationListener;
		this.locationManager = (LocationManager) context.getApplicationContext()
	            .getSystemService(Context.LOCATION_SERVICE);
	}

	public Location getLastLocation(){
		return getLocationByProvider(LocationManager.NETWORK_PROVIDER);
	}
	
	public Location getBestLocation() {
	    Location gpslocation = getLocationByProvider(LocationManager.GPS_PROVIDER);
	    Location networkLocation = getLocationByProvider(LocationManager.NETWORK_PROVIDER);
	    if (gpslocation == null) {
	        Log.d(TAG, "No GPS Location available.");
	        return networkLocation;
	    }
	    if (networkLocation == null) {
	        Log.d(TAG, "No Network Location available");
	        return gpslocation;
	    }
	    long old = System.currentTimeMillis() - checkInterval;
	    boolean gpsIsOld = (gpslocation.getTime() < old);
	    boolean networkIsOld = (networkLocation.getTime() < old);
	    if (!gpsIsOld) {
	        Log.d(TAG, "Returning current GPS Location");
	        return gpslocation;
	    }
	    if (!networkIsOld) {
	        Log.d(TAG, "GPS is old, Network is current, returning network");
	        return networkLocation;
	    }
	    if (gpslocation.getTime() > networkLocation.getTime()) {
	        Log.d(TAG, "Both are old, returning gps(newer)");
	        return gpslocation;
	    } else {
	        Log.d(TAG, "Both are old, returning network(newer)");
	        return networkLocation;
	    }
	}

	public Location getLocationByProvider(String provider) {
	    Location location = null;
	    try {
	        if (locationManager.isProviderEnabled(provider)) {
	            location = locationManager.getLastKnownLocation(provider);
	        }
	    } catch (IllegalArgumentException e) {
	        Log.d(TAG, "Cannot acces Provider " + provider);
	    }
	    return location;
	}

	@SuppressLint("InlinedApi") 
	public void update(){
		Criteria myCriteria = new Criteria();
		myCriteria.setPowerRequirement(Criteria.POWER_LOW);
		String myProvider = locationManager.getBestProvider(myCriteria, true); 

		locationManager.requestLocationUpdates(myProvider, checkInterval, 10000, locationListener);
	}
	
	public void removeUpdate(){
		locationManager.removeUpdates(locationListener);
	}
}
