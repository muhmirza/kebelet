package id.clorus.kebelet.location;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

public class GoogleGeoLocationService {

	public static final String TAG = "GoogleLocationService";

    public static String getLocationString(double latitude, double longitude){
		String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        String jsonData = restTemplate.getForObject(url, String.class);
		try {
			System.out.println(jsonData);
			StringBuilder stringBuilder = new StringBuilder();
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray jsonArray = jsonObject.getJSONArray("results");
			JSONObject result = jsonArray.getJSONObject(0);
			JSONArray components = result.getJSONArray("address_components");
			for(int i=0; i<components.length() ;i++){
				JSONObject component = components.getJSONObject(i);
				ArrayList<String> stringArray = new ArrayList<String>();
				JSONArray types = component.getJSONArray("types");
				for(int j=0; j< types.length();j++){
			        stringArray.add(types.getString(j));
				}
				String longName = component.getString("long_name");
				String string = stringArray.get(0);
				if(string.equals("administrative_area_level_3")){
					stringBuilder.append(longName+", ");
				}
				if(string.equals("administrative_area_level_2")){
					stringBuilder.append(longName+", ");
				}
				if(string.equals("administrative_area_level_1")){
					stringBuilder.append(longName+", ");
				}
				if(string.equals("country")){
					stringBuilder.append(longName+"");
				}
			}
			return stringBuilder.toString();
		} catch (JSONException e) {
			Log.d(TAG, "and error occured while retrieve address, json error");
		}
		return null;
	}
	
	
}
