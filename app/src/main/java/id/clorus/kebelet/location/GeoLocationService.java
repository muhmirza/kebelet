package id.clorus.kebelet.location;


import id.clorus.kebelet.MainActivity;
import id.clorus.kebelet.data.model.googleGeoLocation.GeoLocation;
import id.clorus.kebelet.data.remote.LocationAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mirza on 03/04/16.
 */
public class GeoLocationService {

    private static String alamat;

    private MainActivity mainActivity;


    public static final String TAG = "GeoLocationService";

    public GeoLocationService(){

    }

    public GeoLocationService(MainActivity currentActivity){
        this.mainActivity = currentActivity;
    }

    public String getLocationString(double latitude, double longitude) {


        LocationAPI.Factory.getInstance().getLocation("json?latlng=" + latitude + "," + longitude).enqueue(new Callback<GeoLocation>() {

            String adm3 = null, adm2 = null, adm1 = null, country = null;

            @Override
            public void onResponse(Call<GeoLocation> call, Response<GeoLocation> response) {

                   for (int i = 0; i < response.body().getResults().size(); i++) {
                        for (int x = 0; x < response.body().getResults().get(i).getTypes().size(); x++) {

                             if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_3")) {
                                  //Log.i("3", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                                  adm3 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                             }

                             if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_2")) {
                                 //Log.i("2", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                                 adm2 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                             }

                             if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_1")) {
                                //Log.i("1", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                                adm1 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                             }

                             if (response.body().getResults().get(i).getTypes().get(x).equals("country")) {
                                 //Log.i("country", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                                 country = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                             }

                        }
                   }
                   GeoLocationService.alamat = (adm3+","+adm2+","+adm1+","+country);

            }


            @Override
            public void onFailure(Call<GeoLocation> call, Throwable t) {

            }

        }

        );


        return alamat;
    }




    public void setLocationString(double latitude, double longitude) {


        LocationAPI.Factory.getInstance().getLocation("json?latlng=" + latitude + "," + longitude).enqueue(new Callback<GeoLocation>() {

            String adm3 = null, adm2 = null, adm1 = null, country = null;

            @Override
            public void onResponse(Call<GeoLocation> call, Response<GeoLocation> response) {

                for (int i = 0; i < response.body().getResults().size(); i++) {
                    for (int x = 0; x < response.body().getResults().get(i).getTypes().size(); x++) {

                        if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_3")) {
                            //Log.i("3", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                            adm3 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                        }

                        if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_2")) {
                            //Log.i("2", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                            adm2 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                        }

                        if (response.body().getResults().get(i).getTypes().get(x).equals("administrative_area_level_1")) {
                            //Log.i("1", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                            adm1 = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                        }

                        if (response.body().getResults().get(i).getTypes().get(x).equals("country")) {
                            //Log.i("country", " = " + response.body().getResults().get(i).getAddressComponents().get(x).getLongName());
                            country = response.body().getResults().get(i).getAddressComponents().get(x).getLongName();
                        }

                    }
                }
                GeoLocationService.alamat = (adm3+","+adm2+","+adm1+","+country);
                mainActivity.setLocation(GeoLocationService.alamat);

            }


            @Override
            public void onFailure(Call<GeoLocation> call, Throwable t) {

            }

        }

        );


    }

}
