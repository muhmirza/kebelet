package id.clorus.kebelet.view.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import id.clorus.kebelet.fragment.GettingStartedFragment1;
import id.clorus.kebelet.fragment.GettingStartedFragment2;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    public static int totalPage=2;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return GettingStartedFragment1.newInstance(context);
            case 1:
                return GettingStartedFragment2.newInstance(context);
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return totalPage;
    }

}
