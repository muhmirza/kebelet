package id.clorus.kebelet;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.clorus.kebelet.data.model.foursquare.Foursquare;
import id.clorus.kebelet.data.remote.FoursquareAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mirza on 08/04/16.
 */
public class FoodLocationService {

    List<FoodLocation> foodLocations;

    public List<FoodLocation> explore(double lat, double lon, int limit, int offset){
        foodLocations = new ArrayList<>();

        FoursquareAPI.Factory.getInstance().getToilet("explore?client_id=LNYIHORA3AMQLM0J5L0FKXJUQ4KHMNGE0IQFIQZSNOTTHQPX&client_secret=YXWCEGHXVD5X4NYG0IXBE1WYVL0KAEDCIM02A2VF51AE0WFZ&v=20160403&ll=" + lat + "," + lon + "&categoryId=4d4b7105d754a06374d81259&limit="+limit+"&offset="+offset).enqueue(new Callback<Foursquare>() {
            @Override
            public void onResponse(Call<Foursquare> call, Response<Foursquare> response) {

                for (int i = 0; i < response.body().getResponse().getGroups().get(0).getItems().size(); i++) {

                    String foodLocationName = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getName();
                    int foodLocationDistance = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getDistance();
                    String foodLocationAddress = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getFormattedAddress().toString();
                    Double foodLocationLat = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getLat();
                    Double foodLocationLon = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getLng();

                    FoodLocation foodLocation = new FoodLocation(foodLocationName, foodLocationAddress, foodLocationDistance, foodLocationLat, foodLocationLon);
                    //FoodLocation foodLocation = new FoodLocation();

                    Log.i("name foodLocation", " ke " + i + " = " + foodLocation.getFoodLocationName());

                    FoodLocationService.this.foodLocations.add(foodLocation);


                }


               // Log.i("foodLocations isEmpty2", " = " + foodLocations.isEmpty());
               // Log.i("foodLocations size2", " = " + foodLocations.size());


            }

            @Override
            public void onFailure(Call<Foursquare> call, Throwable t) {

            }
        });

        Log.i("foodLocations isEmpty", " = " + foodLocations.isEmpty());
        Log.i("foodLocations size", " = " + foodLocations.size());
    return foodLocations;
    }


}
