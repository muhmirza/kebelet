package id.clorus.kebelet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import id.clorus.kebelet.config.AppPreference;
import id.clorus.kebelet.smartloginlibrary.SmartCustomLoginListener;
import id.clorus.kebelet.smartloginlibrary.SmartLoginBuilder;
import id.clorus.kebelet.smartloginlibrary.SmartLoginConfig;
import id.clorus.kebelet.smartloginlibrary.manager.UserSessionManager;
import id.clorus.kebelet.smartloginlibrary.users.SmartFacebookUser;
import id.clorus.kebelet.smartloginlibrary.users.SmartGoogleUser;
import id.clorus.kebelet.smartloginlibrary.users.SmartUser;


/**
 * Created by root on 02/04/16.
 */
public class LoginActivity extends AppCompatActivity {
    SmartUser currentUser;
    AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentUser = UserSessionManager.getCurrentUser(this);
        String display = "no user";
        if(currentUser != null) {
            if (currentUser instanceof SmartFacebookUser) {
                SmartFacebookUser facebookUser = (SmartFacebookUser) currentUser;
                display = facebookUser.getProfileName() + " (FacebookUser)is logged in";
                Log.i("name", " = " + facebookUser.getProfileName());
                Log.i("email", " = " + facebookUser.getEmail());
                Log.i("Birthday", " = " + facebookUser.getBirthday());
                Log.i("UserId", " = " + facebookUser.getUserId());
                Log.i("ProfileLink", " = " + facebookUser.getProfileLink());



            } else if (currentUser instanceof SmartGoogleUser) {
                display = ((SmartGoogleUser) currentUser).getDisplayName() + " (GoogleUser) is logged in";
            } else {
                display = currentUser.getUsername() + " (Custom User) is logged in";
            }

            finish();
            Intent intent = new Intent();
            intent.setClass(LoginActivity.this, MainActivity.class);
            startActivity(intent);

        } else {

            SmartLoginBuilder loginBuilder = new SmartLoginBuilder();

            //Set facebook permissions
            ArrayList<String> permissions = new ArrayList<>();
            permissions.add("public_profile");
            permissions.add("email");
            permissions.add("user_birthday");
            permissions.add("user_friends");


            Intent intent = loginBuilder.with(getApplicationContext())
                    .setAppLogo(getlogo())
                    .isFacebookLoginEnabled(true)
                    .withFacebookAppId(getString(R.string.facebook_app_id)).withFacebookPermissions(permissions)
                    .isGoogleLoginEnabled(false)
                    .isCustomLoginEnabled(false).setSmartCustomLoginHelper(new SmartCustomLoginListener() {
                        @Override
                        public boolean customSignin(SmartUser user) {
                            //This "user" will have only username and password set.
                            Toast.makeText(LoginActivity.this, user.getUsername() + " " + user.getPassword(), Toast.LENGTH_SHORT).show();
                            return true;
                        }

                        @Override
                        public boolean customSignup(SmartUser newUser) {
                            //Implement your our custom sign up logic and return true if success
                            return true;
                        }

                        @Override
                        public boolean customUserSignout(SmartUser smartUser) {
                            //Implement logout logic
                            return true;
                        }


                    })
                    .build();

            startActivityForResult(intent, SmartLoginConfig.LOGIN_REQUEST);
            //startActivity(intent);

        }
        //loginResult.setText(display);




    }

    private int getlogo() {

            return R.mipmap.ic_launcher;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String fail = "Login Failed";
        if(resultCode == SmartLoginConfig.FACEBOOK_LOGIN_REQUEST){
            SmartFacebookUser user;
            try {
                user = data.getParcelableExtra(SmartLoginConfig.USER);
                String userDetails = user.getProfileName() + " " + user.getEmail() + " " + user.getBirthday();
                //loginResult.setText(userDetails);


                SmartFacebookUser facebookUser = (SmartFacebookUser) currentUser;
                Log.i("nama", " = " + facebookUser.getFirstName());

                finish();
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, MainActivity.class);
                startActivity(intent);


            }catch (Exception e){
                //loginResult.setText(fail);
            }
        }
        else if(resultCode == SmartLoginConfig.GOOGLE_LOGIN_REQUEST){
            SmartGoogleUser user = data.getParcelableExtra(SmartLoginConfig.USER);
            String userDetails = user.getEmail() + " " + user.getBirthday() + " " + user.getAboutMe();
            //loginResult.setText(userDetails);
        }
        else if(resultCode == SmartLoginConfig.CUSTOM_LOGIN_REQUEST){
            SmartUser user = data.getParcelableExtra(SmartLoginConfig.USER);
            String userDetails = user.getUsername() + " (Custom User)";
            //loginResult.setText(userDetails);
        }
        /*else if(resultCode == SmartLoginConfig.CUSTOM_SIGNUP_REQUEST){
            SmartUser user = data.getParcelableExtra(SmartLoginConfig.USER);
            String userDetails = user.getUsername() + " (Custom User)";
            loginResult.setText(userDetails);
        }*/
        else if(resultCode == RESULT_CANCELED){
            //loginResult.setText(fail);
        }

    }

}
