package id.clorus.kebelet.data.remote;

import id.clorus.kebelet.data.model.googleGeoLocation.GeoLocation;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by root on 24/03/16.
 */
public interface LocationAPI {


    String BASE_URL = "http://maps.googleapis.com/maps/api/geocode/";
//    /json?latlng="+latitude+","+longitude 110.4085203:  userLat = -6.9790126
    @GET("")
    Call<GeoLocation> getLocation(@Url String url);

   class Factory {

        private static LocationAPI service;
        public static LocationAPI getInstance(){

            if (service == null) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                        .build();

                service = retrofit.create(LocationAPI.class);
                return service;
            }
            else return service;
        }
    }



}
