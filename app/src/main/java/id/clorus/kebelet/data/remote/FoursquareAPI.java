package id.clorus.kebelet.data.remote;

import id.clorus.kebelet.data.model.foursquare.Foursquare;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by root on 30/03/16.
 */

public interface FoursquareAPI {


    String BASE_URL = "https://api.foursquare.com/v2/venues/";
    //    /json?latlng="+latitude+","+longitude 110.4085203:  userLat = -6.9790126
    @GET("")
    Call<Foursquare> getToilet(@Url String url);

    class Factory {

        private static FoursquareAPI service;
        public static FoursquareAPI getInstance(){

            if (service == null) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                        .build();

                service = retrofit.create(FoursquareAPI.class);
                return service;
            }
            else return service;
        }
    }


}