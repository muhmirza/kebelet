package id.clorus.kebelet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;



public class Splash extends Activity {

	private static final String TAG = "splash screen";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		SplashThread splashThread = new SplashThread();
		splashThread.start();
	}
	
	class SplashThread extends Thread{
		@Override
		public void run() {
			try{
				synchronized (this) {
					wait(3000);
					
				}
			}catch (InterruptedException e) {
				Log.d(TAG, e.getMessage());
			}
			finish();
			Intent intent = new Intent();
			intent.setClass(Splash.this, LoginActivity.class);
			startActivity(intent);
		}
	}

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
