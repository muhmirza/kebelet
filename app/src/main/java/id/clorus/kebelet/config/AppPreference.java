package id.clorus.kebelet.config;

import de.devland.esperandro.annotations.Default;
import de.devland.esperandro.annotations.SharedPreferences;

@SharedPreferences
public interface AppPreference {

    @Default(ofString = "-6.2297465")
    public String latitude();
    public void latitude(String latitude);
    @Default(ofString = "106.829518")
    public String longitude();
    public void longitude(String longitude);
    @Default(ofString = "Using default location. Jakarta, Indonesia.")
    public String name();
    public void setName(String name);
    public String location();
    public void location(String location);
    public boolean serviceRunning();
    public boolean serviceRunning(boolean serviceRunning);
    public boolean notification();
    public boolean notification(boolean notification);

}
