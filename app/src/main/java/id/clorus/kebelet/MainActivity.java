package id.clorus.kebelet;

import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import de.devland.esperandro.Esperandro;
import id.clorus.kebelet.config.AppPreference;
import id.clorus.kebelet.location.GeoLocationService;
import id.clorus.kebelet.location.GoogleGeoLocationService;
import id.clorus.kebelet.location.LocationServiceManager;
import id.clorus.kebelet.smartloginlibrary.manager.UserSessionManager;
import id.clorus.kebelet.smartloginlibrary.users.SmartFacebookUser;
import id.clorus.kebelet.smartloginlibrary.users.SmartUser;
import id.clorus.kebelet.utils.NetworkUtil;


public class MainActivity extends AppCompatActivity implements LocationListener {

    private ImageButton buttonRefresh;
    private TextView locationTextView,userNameTextView;

    private static final String TAG = "GoHome";
    private LocationServiceManager locationServiceManager;

    private double latitude;
    private double longitude;
    private String location;

    private GeoLocationService geoLocationService;
    SmartUser currentUser;


    private SimpleDateFormat dateFormat;
    AppPreference appPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);

        appPreference = Esperandro.getPreferences(AppPreference.class, this);

        currentUser = UserSessionManager.getCurrentUser(this);
        SmartFacebookUser facebookUser = (SmartFacebookUser) currentUser;

        buttonRefresh = (ImageButton) findViewById(R.id.refresh_button);
        locationTextView = (TextView) findViewById(R.id.location_text_view);
        userNameTextView = (TextView) findViewById(R.id.wellcome);

        Typeface helvetica = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Helvetica.otf");
        locationTextView.setTypeface(helvetica);
        userNameTextView.setTypeface(helvetica);

        locationServiceManager = new LocationServiceManager(this, this);

        Location loc = locationServiceManager.getBestLocation();
        if (loc == null) {
            locationServiceManager.update();
        } else {
            locationChanged(loc);
        }

        latitude = Double.parseDouble(appPreference.latitude());
        longitude = Double.parseDouble(appPreference.longitude());
        location = appPreference.location();
        Log.i("userLang = " + longitude, " userLat = " + latitude);

        geoLocationService = new GeoLocationService(this);
        geoLocationService.setLocationString(latitude, longitude);
        Log.i("geoloc"," = "+geoLocationService.getLocationString(latitude,longitude));

        userNameTextView.setText("Hello "+facebookUser.getProfileName());


    }

    public void setLocation(String alamat){
       this.locationTextView.setText(alamat);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    public void onClick(View view) {
        Log.i("button", "clicked");

        int viewId = view.getId();

        if (viewId == R.id.refresh_button) {
            Location location = locationServiceManager.getBestLocation();
            if (location == null) {
                locationServiceManager.update();
            } else {
                locationChanged(location);
            }

        }


        if (viewId == R.id.btn_get_toilet) {

            Intent intent = new Intent();
            intent.setClass(MainActivity.this, FoodLocationListActivity.class);
            startActivity(intent);

        }

        //ListToilet.getListToilet(latitude,longitude);

    }

    public void locationChanged(Location location) {
        latitude  = location.getLatitude();
        longitude = location.getLongitude();

        //updateAlamat();

        appPreference.latitude(latitude + "");
        appPreference.longitude(longitude + "");
        appPreference.location(this.location);

    }

    private void updateAlamat() {
        if(NetworkUtil.isNetworkAvailable(this))
            location = GoogleGeoLocationService.getLocationString(latitude, longitude);

    }

    @Override
    public void onLocationChanged(Location location) {
        location = locationServiceManager.getBestLocation();
        if(location == null){
            locationServiceManager.update();
        }else{
            locationChanged(location);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
