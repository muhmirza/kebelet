package id.clorus.kebelet;

/**
 * Created by mirza on 07/04/16.
 */

public class FoodLocation implements Comparable<FoodLocation>{
    private String foodLocationName;
    private String foodLocationAddress;
    private int foodLocationDistance;
    private Double lat;
    private Double lang;



    public FoodLocation(String name, String address, int distance, double lattitude, double langitude){
        foodLocationName = name;
        foodLocationAddress = address;
        foodLocationDistance = distance;
        lat = lattitude;
        lang = langitude;
    }

    public String getFoodLocationName() {
        return foodLocationName;
    }

    public void setFoodLocationName(String foodLocationName) {
        this.foodLocationName = foodLocationName;
    }

    public String getFoodLocationAddress() {
        return foodLocationAddress;
    }

    public void setFoodLocationAddress(String foodLocationAddress) {
        this.foodLocationAddress = foodLocationAddress;
    }


    public int getFoodLocationDistance() {
        return foodLocationDistance;
    }

    public void setFoodLocationDistance(int foodLocationDistance) {
        this.foodLocationDistance = foodLocationDistance;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLang() {
        return lang;
    }

    public void setLang(Double lang) {
        this.lang = lang;
    }


    @Override
    public int compareTo(FoodLocation another) {
        return this.getFoodLocationDistance() - another.getFoodLocationDistance();
    }
}