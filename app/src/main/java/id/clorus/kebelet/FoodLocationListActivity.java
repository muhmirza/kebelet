package id.clorus.kebelet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.devland.esperandro.Esperandro;
import id.clorus.kebelet.config.AppPreference;
import id.clorus.kebelet.data.model.foursquare.Foursquare;
import id.clorus.kebelet.data.remote.FoursquareAPI;
import id.clorus.kebelet.utils.NetworkUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mirza on 07/04/16.
 */
public class FoodLocationListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "FoodLocationListActivity";
    DilatingDotsProgressBar dilatingDotsProgressBar;

    List<FoodLocation> listItem;

    AppPreference appPreference;
    private double lattitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardview);

        dilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progress);
        //dilatingDotsProgressBar.showNow();

        appPreference = Esperandro.getPreferences(AppPreference.class, this);
        lattitude = Double.parseDouble(appPreference.latitude());
        longitude = Double.parseDouble(appPreference.longitude());

        if(NetworkUtil.isNetworkAvailable(this)){
            new InitDataTask().execute();
        }else{
            //dilatingDotsProgressBar.hideNow();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyRecyclerViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);


    }


    @Override
    protected void onResume() {
        super.onResume();

        ((MyRecyclerViewAdapter) mAdapter).setOnItemClickListener(new MyRecyclerViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);

                /*
                FoodLocation food = listItems.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("name", food.getFoodLocationName());
                bundle.putString("address", food.getFoodLocationAddress());
                bundle.putDouble("lat", food.getLat());
                bundle.putDouble("lng", food.getLang());
                bundle.putDouble("distance", food.getFoodLocationDistance());

                Intent intent = new Intent(FoodLocationListActivity.this, FoodLocationDetail.class);
                intent.putExtras(bundle);
                startActivity(intent);*/

            }
        });
    }


    ArrayList results;

    String toiletName = null, toiletAddress=null;
    int toiletDistance=0;
    Double lat=0.0, lang=0.0;
    private ArrayList<FoodLocation> getDataSet() {
        results = new ArrayList<FoodLocation>();
        /*for (int index = 0; index < 10; index++) {
            FoodLocation obj = new FoodLocation(null,null,0,0,0);
            results.add(index, obj);
        }*/

        FoursquareAPI.Factory.getInstance().getToilet("explore?client_id=LNYIHORA3AMQLM0J5L0FKXJUQ4KHMNGE0IQFIQZSNOTTHQPX&client_secret=YXWCEGHXVD5X4NYG0IXBE1WYVL0KAEDCIM02A2VF51AE0WFZ&v=20160403&ll=" + lattitude + "," + longitude + "&categoryId=4d4b7105d754a06374d81259&limit=10").enqueue(new Callback<Foursquare>() {
            @Override
            public void onResponse(Call<Foursquare> call, Response<Foursquare> response) {

                for (int i = 0; i < response.body().getResponse().getGroups().get(0).getItems().size(); i++) {

                    toiletName = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getName();
                    toiletDistance = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getDistance();
                    toiletAddress = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getFormattedAddress().toString();
                    lat = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getLat();
                    lang = response.body().getResponse().getGroups().get(0).getItems().get(i).getVenue().getLocation().getLng();

                    //ToiletItem toilet = new ToiletItem(toiletName, toiletDistance, toiletAddress, lat, lang);
                    FoodLocation toilet = new FoodLocation(toiletName,toiletAddress,toiletDistance,lat,lang);
                    results.add(i, toilet);


                    Log.i("nama toilet", " = " + toiletName);
                    Log.i("alamat toilet", " = " + toiletAddress);
                    Log.i("jarak toilet", " = " + toiletDistance + " meter");
                    Log.i("LatLng toilet", " = " + lat + "," + lang);

                }

            }

            @Override
            public void onFailure(Call<Foursquare> call, Throwable t) {

            }
        });



        return results;
    }

    private class InitDataTask extends AsyncTask<Void, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                if (isCancelled()) {
                    return false;
                }
                updateData(10);
                Log.i("list isEmpty", " = " + listItem.isEmpty());
                return true;
            }catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {
                //dilatingDotsProgressBar.hideNow();
                //dilatingDotsProgressBar.setVisibility(View.GONE);


                super.onPostExecute(result);
            }
        }

        @Override
        protected void onCancelled() {

        }
    }



    private void updateData(int size) {
        FoodLocationService foodLocation = new FoodLocationService();
        List<FoodLocation> list = foodLocation.explore(lattitude, longitude, 10, size);

        Log.i("list isEmpty", " = " + listItem.isEmpty());
        Collections.sort(list);
        listItem.addAll(list);

    }

}