package id.clorus.kebelet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.devland.esperandro.Esperandro;
import id.clorus.kebelet.config.AppPreference;
import id.clorus.kebelet.view.viewpager.ViewPagerAdapter;


public class Slider extends AppCompatActivity {

    public static final int ADD_KEYWORD_CODE = 2;

    private AppPreference appPreference;
    private ProgressDialog progressDialog;
    private ViewPagerAdapter adapter;


    @Bind(R.id.viewPager)
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreference = Esperandro.getPreferences(AppPreference.class, this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.slider);
        ButterKnife.bind(this);

/*        progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
*/

        adapter = new ViewPagerAdapter(getApplicationContext(), getSupportFragmentManager());
        viewPager.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        //FlurryAgent.init(this, AppConfig.FLURRY_API);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    private void nextActivity(){
        finish();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);

    }
}
